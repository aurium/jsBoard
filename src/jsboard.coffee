do (exports=window, $=jQuery)->

  __dirname = do ->
    for line in Error().stack.split('\n')
      break if /\/jsboard\.js/.test line
    line.replace /.*((https?|file):\/\/.*\/)[^\/]*$/, '$1'

  # Create an HTML element and append it to some other element
  mk = (tag, parent)-> $("<#{tag}></#{tag}>").appendTo(parent)

  class JsBoard

    constructor: (parent='body', config={})->
      parent = $(parent) if 'string' is typeof parent
      parent = parent[0] if parent.jquery?
      throw Error 'No parent defined.' unless parent?
      @_w = parseInt config.width or 600
      @_h = parseInt config.height or 400
      @_brushSize = config.brushSize or 30
      @_fillColor = config.fillColor or '#080'
      idNum = Math.random().toString(16).split('.')[1].toUpperCase()
      @baseEl = mk('div', parent).attr(id: 'jsBoard-'+idNum)[0]
      @_canvas = mk('canvas', @baseEl) #.attr width: @_w, height: @_h
      @_canvas.on 'mousedown', (ev)=> mouseDown this, ev
      $(window).on 'mouseup',  (ev)=> mouseUp this, ev
      @_canvas.on 'mousemove', (ev)=> mouseMove this, ev
      @_ctx = @_canvas[0].getContext '2d'
      $(window).on 'resize', (ev)=> windowResize this
      $(document).on 'webkitfullscreenchange mozfullscreenchange fullscreenchange', (ev)=> fullscreenChange this
      $(document).on 'webkitfullscreenerror mozfullscreenerror fullscreenerror', (ev)=> fullscreenError this
      @_mouseBt = []
      @_ctrl = mk('div', @baseEl).addClass 'jsBoard-ctrl'
      mkCtrl this
      mk('style', 'html>head').text mkStileSheet this
      @resize width: @_w, height: @_h
      do @updateBrush

    toggleCtrlLock: -> $(@baseEl).toggleClass 'ctrl-locked'

    fullScreen: (doIt)->
      if doIt
        console.log 'fullScreen ON!'
        @baseEl.requestFullscreen ?= @baseEl.mozRequestFullScreen
        @baseEl.requestFullscreen ?= @baseEl.webkitRequestFullscreen
        if @baseEl.requestFullscreen?
          @_isFullScreen = true
          @baseEl.requestFullscreen()
        else
          @_isFullScreen = false
      else
        console.log 'fullScreen OFF'
        @_isFullScreen = false
        @resize @_lastOnPageSize

    resize: (size)->
      imgData = @_ctx.getImageData 0, 0, @_w, @_h
      @_w = size.width or @_w
      @_h = size.height or @_h
      @_lastOnPageSize = width: @_w, height: @_h unless @_isFullScreen
      @baseEl.style.width = @_w + 'px'
      @baseEl.style.height = @_h + 'px'
      @_canvas.attr width: @_w, height: @_h
      @_ctx.putImageData imgData, 0, 0
      do @updateBrush

    setFillColor: (@_fillColor)-> do @updateBrush

    incBrush: (inc)->
      @_brushSize += inc
      @_brushSize = 50 if isNaN @_brushSize
      @_brushSize = 100 if @_brushSize > 100
      @_brushSize = 1 if @_brushSize < 1
      @_brushSize = Math.round @_brushSize
      do @updateBrush

    updateBrush: ->
      displaySize = Math.round (@_brushSize+1)/2
      @_brushCtrlPoint.css
        width: displaySize + 'px'
        height: displaySize + 'px'
        left: (50-displaySize)/2 + 'px'
        top: (50-displaySize)/2 + 'px'
        background: @_fillColor
      @_brushCtrlInfo.text @_brushSize + 'px'
      @_ctx.lineWidth = @_brushSize
      @_ctx.lineJoin = 'round'
      @_ctx.lineCap = 'round'

    moveTo: (@_currentPosition)->
      @_ctx.beginPath()
      @_ctx.moveTo @_currentPosition.x, @_currentPosition.y

    lineTo: (point)->
      @_ctx.lineTo point.x, point.y
      @_ctx.strokeStyle = @_fillColor
      @_ctx.stroke()
      @_ctx.closePath()
      @_ctx.moveTo point.x, point.y

    drawLine: (points)->
      @moveTo x:points.x1, y:points.y1
      @lineTo x:points.x2, y:points.y2


  windowResize = (board)->
    if board._isFullScreen
      doc = $(document)
      board.resize width: doc.width(), height: doc.height()
    board._pageOffset = $(board.baseEl).offset()

  fullscreenChange = (board)->
    docIsFullScreen = document.fullscreen or document.mozFullScreen or document.webkitIsFullScreen
    console.log 'fullscreenChange', board._isFullScreen, docIsFullScreen
    if board._isFullScreen and not docIsFullScreen
      board.fullScreen false

  fullscreenError = (board)->
    if board._isFullScreen
      board.fullScreen false

  mouseDown = (board, ev)->
    {top, left} = board._pageOffset
    board._mouseBt[ev.button] = true
    board.moveTo x:ev.pageX-left, y:ev.pageY-top if board._mouseBt[0]

  mouseUp = (board, ev)->
    {top, left} = board._pageOffset
    board.lineTo x:ev.pageX-left, y:ev.pageY-top if board._mouseBt[0]
    board._mouseBt[ev.button] = false

  mouseMove = (board, ev)->
    {top, left} = board._pageOffset
    board.lineTo x:ev.pageX-left, y:ev.pageY-top if board._mouseBt[0]

  mkCtrl = (board)->
    throw Error 'Undefined jsBoard' unless board?
    mk('locker', board._ctrl).on 'click', -> do board.toggleCtrlLock
    board._brushCtrl = mk('brush', board._ctrl)
    board._brushCtrlPoint = mk('point', board._brushCtrl)
    board._brushCtrlInfo = mk('span', board._brushCtrl)
    mk('more', board._brushCtrl).text('+').on 'click', -> board.incBrush 4
    mk('less', board._brushCtrl).text('-').on 'click', -> board.incBrush -4
    # make a palete:
    for i in [0..11]
      do (i=i)->
        color = "hsl(#{i*30},100%,25%)"
        mk('p', board._ctrl)
        .addClass 'jsBoard-palete-line1 jsBoard-palete-index'+i
        .css 'background-color': color
        .on 'click', -> board.setFillColor color
      do (i=i)->
        color = "hsl(#{i*30},100%,50%)"
        mk('p', board._ctrl)
        .addClass 'jsBoard-palete-line2 jsBoard-palete-index'+i
        .css 'background-color': color
        .on 'click', -> board.setFillColor color
      do (i=i)->
        color = "hsl(#{i*30},100%,75%)"
        mk('p', board._ctrl)
        .addClass 'jsBoard-palete-line3 jsBoard-palete-index'+i
        .css 'background-color': color
        .on 'click', -> board.setFillColor color
    for i in [0..6]
      do (i=i)->
        color = "hsl(0,0%,#{Math.round 100*(i)/6}%)"
        mk('p', board._ctrl)
        .addClass 'jsBoard-palete-line4 jsBoard-palete-index'+i
        .css 'background-color': color
        .on 'click', -> board.setFillColor color

  mkStileSheet = (board)->
    throw Error 'Undefined jsBoard' unless board?
    baseId = board.baseEl.id
    css = {
      '':
        background: '#FFF'
        position: 'relative'
        overflow: 'hidden'
        'box-shadow': '0 0 20px rgba(0,0,0,0.4)'
      '.jsBoard-ctrl':
        position: 'absolute'
        right: 0
        bottom: 0
        background: "rgba(200,200,200,0.5) url(#{__dirname}imgs/gear.svg) 80% 80% no-repeat"
        border: '1px solid rgba(0,0,0,0.4)'
        'border-right': 'none'
        'border-bottom': 'none'
        'border-radius': '100% 0 0 0'
        width: '30px'
        height: '30px'
        transition: '1s'
      ".jsBoard-ctrl:hover, &.ctrl-locked .jsBoard-ctrl":
        width: '220px'
        height: '220px'
        transition: '0.5s'
        'background-position': '110% 110%'
      '.jsBoard-ctrl locker':
        display: 'block'
        width: '20px'
        height: '20px'
        position: 'absolute'
        right: '-20px'
        bottom: '3px'
        background: "url(#{__dirname}imgs/locker.svg) 100%"
        opacity: 0.4
        cursor: 'pointer'
        transition: 'right 1s'
      "&.ctrl-locked .jsBoard-ctrl locker":
        'background-position': 0
      ".jsBoard-ctrl:hover locker, &.ctrl-locked .jsBoard-ctrl locker":
        right: '3px'
        bottom: '3px'
        transition: 'right 0.5s'
      '.jsBoard-ctrl brush':
        display: 'block'
        width: '50px'
        height: '50px'
        position: 'absolute'
        left: '50px'
        bottom: '-10px'
        opacity: 0
        transition: '0.8s'
      '.jsBoard-ctrl:hover brush, &.ctrl-locked .jsBoard-ctrl brush':
        left: '130px'
        bottom: '6px'
        opacity: 1
        transition: '0.3s'
      '.jsBoard-ctrl brush point':
        position: 'absolute'
        left: 0
        botom: 0
        display: 'block'
        width: '30px'
        height: '30px'
        border: '1px solid rgba(0,0,0,0.2)'
        'border-radius': '50% 50% 50% 0'
        transition: 'background 0.5s'
      '.jsBoard-ctrl brush span':
        position: 'absolute'
        right: '-7px'
        bottom: '1px'
        font: '13px sans-serif'
        color: '#000'
        'text-shadow': '0 0 2px #EEE, 0 0 2px #EEE, 0 0 2px #EEE, 0 0 2px #EEE'
        'white-space': 'nowrap';
      '.jsBoard-ctrl brush:before':
        content: '""'
        position: 'absolute'
        left: '15px'
        top: '-9px'
        border: '1px solid rgba(0,0,0,0.2)'
        display: 'block'
        width: '80px'
        height: '10px'
        'border-radius': '50%'
        background: '#A74'
        transform: 'rotate(-45deg)'
      '.jsBoard-ctrl brush more, .jsBoard-ctrl brush less':
        position: 'absolute'
        font: '1px sans-serif'
        color: 'transparent'
        opacity: 0.5
        'border-radius': '10px'
        display: 'block'
        width: '30px'
        height: '30px'
        cursor: 'pointer'
      '.jsBoard-ctrl brush:hover more, .jsBoard-ctrl brush:hover less':
        background: 'rgba(255,255,255,0.8)'
      '.jsBoard-ctrl brush more:before, .jsBoard-ctrl brush less:before':
        content: '""'
        background: '#000'
        'border-radius': '3px'
        display: 'block'
        width: '24px'
        height: '6px'
        position: 'absolute'
        top: '12px'
        left: '3px'
      '.jsBoard-ctrl brush more:after':
        content: '""'
        background: '#000'
        'border-radius': '3px'
        display: 'block'
        width: '6px'
        height: '24px'
        position: 'absolute'
        top: '3px'
        left: '12px'
      '.jsBoard-ctrl brush more':
        right: '-1px'
        bottom: '44px'
      '.jsBoard-ctrl brush less':
        right: '-27px'
        bottom: '18px'
      '.jsBoard-ctrl p':
        position: 'absolute'
        display: 'block'
        width: '20px'
        height: '20px'
        margin: 0
        right: '-20px'
        bottom: '-20px'
        border: '1px solid rgba(0,0,0,0.2)'
        'border-radius': '50%'
        cursor: 'pointer'
        transition: '1s'
      ".jsBoard-ctrl:hover p, &.ctrl-locked .jsBoard-ctrl p":
        transition: '0.5s'
    }
    mkSelPalete = (index, line)->
      ".jsBoard-ctrl:hover X, &.ctrl-locked .jsBoard-ctrl X"
      .replace /X/g, ".jsBoard-palete-index#{index}.jsBoard-palete-line#{line}"
    for i in [0..11]
      css[mkSelPalete i, 1] =
        right: Math.round(180 * Math.cos i*.143) + 'px'
        bottom: Math.round(180 * Math.sin i*.143) + 'px'
        width: '28px'
        height: '28px'
      css[mkSelPalete i, 2] =
        right: Math.round(153 * Math.cos i*.143) + 'px'
        bottom: Math.round(153 * Math.sin i*.143) + 'px'
        width: '26px'
        height: '26px'
      css[mkSelPalete i, 3] =
        right: Math.round(128 * Math.cos i*.143) + 'px'
        bottom: Math.round(128 * Math.sin i*.143) + 'px'
        width: '24px'
        height: '24px'
      css[mkSelPalete i, 4] =
        right: Math.round(105 * Math.cos i*.261) + 'px'
        bottom: Math.round(105 * Math.sin i*.261) + 'px'
    (for sel, block of css
      sel = sel.replace(/(^|,)/g, "$1 ##{baseId} ").replace /\s*&/g, ''
      "#{sel} {#{(att+':'+val+';' for att,val of block).join '\n'}}"
    ).join '\n\n'

  exports.buildJsBoard = (parent='body', config={}, afterCreate=(->))->
    try
      board = new JsBoard parent, config
      setTimeout (->
        windowResize board
        afterCreate null, board
      ), 500 # force first adjustment
      board
    catch err
      afterCreate err
